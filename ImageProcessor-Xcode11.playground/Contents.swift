import UIKit

protocol Filter {
    func process(image: UIImage) -> UIImage
}

extension UIImage {
    func apply(filter: Filter) -> UIImage {
        return filter.process(image: self)
    }

    func averageColor() -> (Int, Int, Int) {
        var totalRed = 0
        var totalGreen = 0
        var totalBlue = 0

        let rgbaImage = RGBAImage(image:self)!
        for x in 0..<rgbaImage.width {
            for y in 0..<rgbaImage.height {
                let index = y * rgbaImage.width + x
                let pixel = rgbaImage.pixels[index]
                
                totalRed += Int(pixel.red)
                totalGreen += Int(pixel.green)
                totalBlue += Int(pixel.blue)
            }
        }

        let pixelCount = rgbaImage.width * rgbaImage.height
        let avgRed = totalRed / pixelCount
        let avgGreen = totalGreen / pixelCount
        let avgBlue = totalBlue / pixelCount
        
        return (avgRed, avgGreen, avgBlue)
    }
}

func clampValue(_ value:Int, between minValue: Int, and maxValue: Int) -> Int {
    return max(minValue, min(maxValue, value))
}

struct RgbFilter: Filter {
    let redFactor: Int
    let greenFactor: Int
    let blueFactor: Int

    func process(image: UIImage) -> UIImage {
        let (avgGreen, avgRed, avgBlue) = image.averageColor()
        
        let rgbaImage = RGBAImage(image:image)!
        for x in 0..<rgbaImage.width {
            for y in 0..<rgbaImage.height {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                let redDiff = Int(pixel.red) - avgRed
                let greenDiff = Int(pixel.green) - avgGreen
                let blueDiff = Int(pixel.blue) - avgBlue

                pixel.red = (redDiff > 0)
                    ? UInt8(clampValue((avgRed + redDiff * redFactor), between: 0, and: 255))
                    : UInt8(clampValue((avgRed + redDiff / redFactor), between: 0, and: 255))

                pixel.green = (greenDiff > 0)
                    ? UInt8(clampValue((avgGreen + greenDiff * greenFactor), between: 0, and: 255))
                    : UInt8(clampValue((avgGreen + greenDiff / greenFactor), between: 0, and: 255))
                
                pixel.blue = (blueDiff > 0)
                    ? UInt8(clampValue((avgBlue + blueDiff * blueFactor), between: 0, and: 255))
                    : UInt8(clampValue((avgBlue + blueDiff / blueFactor), between: 0, and: 255))

                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage.toUIImage()!
    }
}

struct BwFilter: Filter {
    let redFactor: Double
    let greenFactor: Double
    let blueFactor: Double
    
    // Factors based on https://www.kdnuggets.com/2019/12/convert-rgb-image-grayscale.html
    init(redFactor: Double = 0.299, greenFactor: Double = 0.587, blueFactor: Double = 0.114) {
        self.redFactor = redFactor
        self.greenFactor = greenFactor
        self.blueFactor = blueFactor
    }
    
    func process(image: UIImage) -> UIImage {
        let rgbaImage = RGBAImage(image:image)!
        for x in 0..<rgbaImage.width {
            for y in 0..<rgbaImage.height {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let avgColor = Double(pixel.red)  * redFactor
                        + Double(pixel.green)  * greenFactor
                        + Double(pixel.blue) * blueFactor

                let clampedColor = UInt8(clampValue(Int(avgColor), between: 0, and: 255))
                
                pixel.red = UInt8(clampValue(Int(clampedColor), between: 0, and: 255))
                pixel.green = UInt8(clampValue(Int(clampedColor), between: 0, and: 255))
                pixel.blue = UInt8(clampValue(Int(clampedColor), between: 0, and: 255))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage.toUIImage()!
    }
}

struct BrightnessFilter: Filter {
    let brightness: Double
    
    init(brightness: Double = 1.0) {
        self.brightness = brightness
    }
    
    func process(image: UIImage) -> UIImage {
        let rgbaImage = RGBAImage(image:image)!

        for x in 0..<rgbaImage.width {
            for y in 0..<rgbaImage.height {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                pixel.red = UInt8(clampValue(Int(Double(pixel.red) * brightness), between: 0, and: 255))
                pixel.green = UInt8(clampValue(Int(Double(pixel.green) * brightness), between: 0, and: 255))
                pixel.blue = UInt8(clampValue(Int(Double(pixel.blue) * brightness), between: 0, and: 255))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage.toUIImage()!
    }
}

struct ContrastFilter: Filter {
    let contrast: Double
    
    init(contrast: Double = 1.0) {
        self.contrast = contrast
    }
    
    func process(image: UIImage) -> UIImage {
        let (avgGreen, avgRed, avgBlue) = image.averageColor()
        let avgColor = (avgGreen + avgRed + avgBlue) / 3
        
        let rgbaImage = RGBAImage(image:image)!
        for x in 0..<rgbaImage.width {
            for y in 0..<rgbaImage.height {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                let redDiff = Int(pixel.red) - avgColor
                let greenDiff = Int(pixel.green) - avgColor
                let blueDiff = Int(pixel.blue) - avgColor

                pixel.red = UInt8(clampValue((avgRed + Int(Double(redDiff) * contrast)), between: 0, and: 255))
                pixel.green = UInt8(clampValue((avgGreen + Int(Double(greenDiff) * contrast)), between: 0, and: 255))
                pixel.blue = UInt8(clampValue((avgBlue + Int(Double(blueDiff) * contrast)), between: 0, and: 255))

                rgbaImage.pixels[index] = pixel
            }
        }
        
        return rgbaImage.toUIImage()!
    }
}

func diffImage(a: UIImage, b: UIImage) -> Bool{
    let rgbA = RGBAImage(image:a)!
    let rgbB = RGBAImage(image:b)!
    
    guard rgbA.height == rgbB.height && rgbA.width == rgbB.width else {
        print("Images are different")
        return false
    }
    
    let height = rgbA.height
    let width = rgbA.width
    
    var differs = false
    for x in 0..<width {
        for y in 0..<height {
            let index = y * width + x
            
            let pixelA = rgbA.pixels[index]
            let pixelB = rgbB.pixels[index]
            
            let diffR = pixelA.red - pixelB.red
            let diffG = pixelA.green - pixelB.green
            let diffB = pixelA.blue - pixelB.blue
            
            if diffR != 0 || diffG != 0 || diffB != 0 {
                print("Pixel \(index) differs by (\(diffR), \(diffG), \(diffB))")
                differs = true
                break
            }
        }
    }
    
    if !differs {
        print("Images are the same")
        return true
    }
    
    return false
}

enum FilterEnum: String {
    case rgbFilter = "Default RGB"
    case bwFilter = "Default B&W"
    case linearBwFilter = "Linear B&W"
    case perc50BrightFilter = "50% Brightness"
    case perc50ContrastFilter = "50% Contrast"
    case perc150BrightFilter = "150% Brightness"
    case perc150ContrastFilter = "150% Contrast"
}

class ImageProcessor {
    static let rgbFilter = RgbFilter(redFactor: 1, greenFactor: 1, blueFactor: 1)
    static let bwFilter = BwFilter()
    static let linearBwFilter = BwFilter(redFactor: 1.0, greenFactor: 1.0, blueFactor: 1.0)
    static let fiftyPercentBrightnessFilter = BrightnessFilter(brightness: 0.5)
    static let fiftyPercentContrastFilter = ContrastFilter(contrast: 0.5)
    static let hundredFiftyPercentContrastFilter = ContrastFilter(contrast: 1.5)
    static let hundredFiftyPercentBrightnessFilter = BrightnessFilter(brightness: 1.5)

    var filterChain = [Filter]()

    func appendFilter(_ name: String) {
        switch FilterEnum(rawValue: name) {
        case .rgbFilter: filterChain.append(ImageProcessor.rgbFilter)
        case .bwFilter: filterChain.append(ImageProcessor.bwFilter)
        case .linearBwFilter: filterChain.append(ImageProcessor.linearBwFilter)
        case .perc50BrightFilter: filterChain.append(ImageProcessor.fiftyPercentBrightnessFilter)
        case .perc50ContrastFilter: filterChain.append(ImageProcessor.fiftyPercentContrastFilter)
        case .perc150BrightFilter: filterChain.append(ImageProcessor.hundredFiftyPercentBrightnessFilter)
        case .perc150ContrastFilter: filterChain.append(ImageProcessor.hundredFiftyPercentContrastFilter)
        default: break
        }
    }
    
    func appendFilters(_ names: [String]) {
        for name in names {
            appendFilter(name)
        }
    }
    
    func apply(image: UIImage) -> UIImage {
        var processed = image
        for filter in filterChain {
            processed = processed.apply(filter: filter)
        }
        return processed
    }
}

let image = UIImage(named: "sample")!

// Applies chained filters to UIImage
let newImage1 = image
    .apply(filter: RgbFilter(redFactor: 2, greenFactor: 1, blueFactor: 1))
    .apply(filter: ImageProcessor.hundredFiftyPercentContrastFilter)

// Specifies a chain of filters then applies
let processor = ImageProcessor()
processor.appendFilter("Default B&W")
processor.filterChain += [ContrastFilter(contrast: 1.25)]
let newImage2 = processor.apply(image: image)

